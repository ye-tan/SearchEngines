明世宗朱厚熜（1507年9月16日－1567年1月23日），明朝第十一位皇帝，明宪宗庶孙，明孝宗之侄，明武宗堂弟，兴献王朱佑杬次子。由于武宗死后无嗣，因此张皇太后（明武宗的母亲）和内阁首辅杨廷和决定，由近支的皇室、武宗的堂弟朱厚熜继承皇位。1521年（嘉靖元年）--1566年（嘉靖四十五年）在位，（在位时间之长在明代皇帝中仅次于其孙子明神宗），年号嘉靖，与杨廷和等朝臣在议父兴献王尊号的问题上发生争论，史称“大礼议”之争。
　　
　　早期整顿朝纲、减轻赋役，对外抗击倭寇，后史誉之谓“中兴时期”。后期崇信道教，并痴迷于炼丹，致使后来发生“壬寅宫变”，之后不再理政。嘉靖四十五年（1566年）驾崩，终年60岁。庙号世宗，谥号钦天履道英毅神圣宣文广武洪仁大孝肃皇帝。葬于北京明十三陵之永陵。
　　
　　明世宗在位早期英明苛察，严以驭官，宽以治民，可以称得上是为有作为的皇帝，但是他中后期任用奸佞，妄杀忠良，宠好道教，信任方士，导致明朝国势日益衰微
　　
　　嘉靖四十五年(1566年)十二月十四日，朱厚熜卒于乾清宫，年60岁。庙号世宗。葬北京昌平永陵。
　　
　　公元1566年，明嘉靖四十五年十二月十五日，裕王朱载垕继位，年号隆庆。奉先帝世宗皇帝遗诏，“存者召用，殁者恤录，见监者即先释放复职”。以海瑞为代表，赦免了所有谏言诸臣。从这一刻起，揭开了长达十八年隆庆大改革的序幕！
　　
　　人物
　　
　　在嘉靖时期出现了夏言、张璁、严嵩、徐阶、高拱等人。他们都掌控着国家大权。在这里面的严嵩可是明代第一奸臣。徐阶是一位非常厉害的政治家，他成功斗败严嵩，挽救大明王朝于危局。
　　
　　壬寅宫变
　　
　　明朝嘉靖年间的“壬寅宫变”，是历史上一起罕见的宫女起义。当时明世宗嘉靖皇帝朱厚熜为求长生不老药，提倡以“吸风饮露之道”成仙，。当时古人有以蕉叶待露的习俗。在园中可植蕉数株，每早，阔叶上必布满甘露，晨起口干舌燥之即，吮吸若干片，可觉甘甜爽口，并有延年宜寿之说。这也只合生在环境尚未污染之时的古人使用，今人断不可取，盖今日之晨露，已囊括了多种金属重粒子矣。可惜明世宗嘉靖帝不懂此法，此人对修道成仙已近于癫狂，为采集甘露饮用，日命宫女们凌晨即往御花园中采露，导致大量宫女因之累倒病倒，遂演壬寅宫变。杨金英等十数名宫女用黄绫布几乎把这位皇帝勒死。大约北京城种不得这江南之物，故世宗没有想到用蕉叶来替代盛露的玉盘吧！
　　
　　公元1542年12月21日深夜，以杨金英为首的宫女们决定起义趁嘉靖帝熟睡之时，用麻绳希望勒毙他。谁知在慌乱之下，宫女们将麻绳打成死结，结果只令嘉靖帝吓昏，而未有毙命。在这时其中一个胆小的宫女因害怕，报告给方皇后。方皇后马上领人来救驾，将宫女们制服，嘉靖帝被救下后吐血数升，但大难不死，宫女们全部被捕，几天后被凌迟处死。而且，连当时服侍嘉靖帝的端妃，也一并斩首。由于此事发生在在嘉靖壬寅年（嘉靖二十一年，公元1542年），所以后世史学家称之为“壬寅宫变”